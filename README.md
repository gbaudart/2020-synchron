# Presentation SYNCHRON 2020

Interfacing a synchronous language with Python and Jupyter Notebooks

- Guillaume Baudart (Inria Paris),
- Adrien Guatto (Université de Paris),
- Louis Mandel (IBM Research, USA) 

## Outline
1. High-Level Interactions (example audio)
2. Low-Level Interactions (example rppl)
3. Python and C Codegen (example pendulum)


## Getting Started

To play with the notebooks you need Python 3, a few packages, and a experimental branch of Zelus.

```
git clone https://github.com/INRIA/zelus
cd zelus
git checkout python
opam pin -k path .
cd ..
git clone https://gitlab.inria.fr/gbaudart/2020-synchron.git
cd 2020-synchron
pip install -r requirements.txt
```

You can then launch the jupyter server (from the root of `2020-synchron`) which should open a webpage on your browser listing all the files.
```
jupyter notebook
```

The three parts are:
```
- 2020-synchron-part-1-audio.ipynb
- 2020-synchron-part-2-rppl.ipynb
- 2020-synchron-part-3-heptagon.ipynb
```

A self-contained html version of these notebooks can be found in the `html` directory.